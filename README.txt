CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * FAQ
 * Maintainers

INTRODUCTION
------------

Views Filter Menu provides a Views filter which will filter nodes based
on whether they are added to specific menus or not.

Users are provided with an interface which displays all configured menus,
and may filter nodes based on whether they are added or not to specific menus.

Multiple menus may be selected.

REQUIREMENTS
------------

Views (Core Drupal)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/documentation/install/modules-themes/modules-8
   for further information.

CONFIGURATION
-------------

 * Create a view of nodes and then add a filter.
 * Search for 'Menu' and then select 'Menu filter'.
 * Once applied, an interface will be displayed allowing the selection of
   menus, and whether it will filter based on if the nodes
   are 'in' or 'out' of the menus selected.
 * If the 'in' operator is selected, then only nodes that are assigned
   to menus are returned.
 * If the 'not in' operator is selected, then nodes that are not in the
   specified menu(s) are returned along with all nodes that are not
   assigned to any menu at all.   

FAQ
---

* Which entities are supported by this module?
  - This filter currently only filters node entities.

MAINTAINERS
-----------

Current maintainers:
 * Jon Hockley (jonnyhocks) - https://www.drupal.org/u/jonnyhocks
