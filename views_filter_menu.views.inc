<?php

/**
 * @file
 * Views API declaration file.
 */

/**
 * Implements hook_views_data_alter().
 */
function views_filter_menu_views_data_alter(array &$data) {

  $data['node_field_data']['menu_filter'] = [
    'title' => t('Menu filter'),
    'filter' => [
      'title' => t('Menu filter'),
      'help' => t('Provides a custom filter for nodes by their menu.'),
      'field' => 'nid',
      'id' => 'views_filter_menu_node_menus',
    ],
  ];
}
