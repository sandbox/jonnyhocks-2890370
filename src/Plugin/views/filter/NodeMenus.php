<?php

namespace Drupal\views_filter_menu\Plugin\views\filter;

use Drupal\system\Entity\Menu;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\filter\InOperator;
use Drupal\views\ViewExecutable;
use Drupal\views\Views;

/**
 * Class NodeMenus.
 *
 * Filters by given list of menu names.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("views_filter_menu_node_menus")
 * @Views
 */
class NodeMenus extends InOperator {

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->valueTitle = t('Menus');
    $this->definition['options callback'] = [
      $this,
      'generateOptions',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Override the query so that no filtering takes place. if the user
    // doesn't select any options.
    if (!empty($this->value)) {
      $configuration = [
        'table' => 'menu_link_content_data',
        'field' => FALSE,
        'left_table' => FALSE,
        'left_field' => FALSE,
        'operator' => '=',
      ];
      $join = Views::pluginManager('join')->createInstance('menu_node_join', $configuration);
      $this->query->addRelationship('menu_link_content_data', $join, 'node_field_data');
      // If 'not in' is selected, then we also wish to include all nodes that
      // are not assigned to any menu.
      if ($this->operator == 'not in') {
        $condition_obj = new Condition('OR');
        $condition_obj->condition('menu_link_content_data.menu_name', array_keys($this->value), $this->operator);
        $condition_obj->condition('menu_link_content_data.menu_name', array_keys($this->value), 'IS NULL');
        $this->query->addWhere($this->options['group'], $condition_obj);
      }
      else {
        $this->query->addWhere($this->options['group'], 'menu_link_content_data.menu_name', array_keys($this->value), $this->operator);
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    // Skip validation if no options have been chosen so we can use it as a
    // non-filter.
    if (!empty($this->value)) {
      parent::validate();
    }
  }

  /**
   * Generate options for Views UI.
   *
   * @return array
   *   Options for the user to select to filter.
   */
  public function generateOptions() {
    $all_menus = Menu::loadMultiple();
    $menus = [];
    foreach ($all_menus as $id => $menu) {
      $menus[$id] = $menu->label();
    }
    return $menus;
  }

}
