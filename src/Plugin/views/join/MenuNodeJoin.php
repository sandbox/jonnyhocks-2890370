<?php

namespace Drupal\views_filter_menu\Plugin\views\join;

use Drupal\views\Plugin\views\join\JoinPluginBase;

/**
 * Views Join plugin to join the Node table to the menu_tree table.
 *
 * @package Drupal\views_filter_menu
 * @ingroup views_join_handlers
 * @ViewsJoin("menu_node_join")
 */
class MenuNodeJoin extends JoinPluginBase {

  /**
   * Prefix values.
   *
   * The values to concat with node.nid in to join on the menu_link_content_data
   * link__uri column.
   *
   * @var array
   */
  public $prefixes = ['entity:node/'];

  /**
   * Builds the join to add to the query.
   *
   * @param object $select_query
   *   The select query object.
   *   Drupal\Core\Database\Driver\mysql\Select.
   * @param mixed $table
   *   Array of table properties.
   * @param object $view_query
   *   View query object.
   *   Drupal\views\Plugin\views\query\Sql.
   */
  public function buildJoin($select_query, $table, $view_query) {
    // Add an additional hardcoded condition to the query.
    $values = [];

    $node_table_alias = $select_query->getTables()['node_field_data']['alias'];

    $condition = "( $this->table.enabled = 1 ) AND ( ";
    // Loop over the prefixes and construct the JOIN condition SQL.
    for ($i = 0; $i < count($this->prefixes); $i++) {
      // Use MySQL's CONCAT func to concatenate the prefix with the node.nid.
      // Provide the prefix as a parametrized value.
      $placeholder = ':views_join_condition_' . $select_query->nextPlaceholder();
      $values[$placeholder] = $this->prefixes[$i];

      $condition .= "( CONCAT($placeholder, $node_table_alias.nid) = $this->table.link__uri)";
      // Each additional prefix to use in the join condition is added as an
      // "OR" statement.
      // Keep using the OR statement unless we're on the last iteration
      // or there is only one prefix to use.
      if ($i < (count($this->prefixes) - 1)) {
        $condition .= " OR ";
      }
    }
    $condition .= " )";

    $select_query->addJoin($this->type, $this->table, $table['alias'], $condition, $values);
  }

}
